import java.util.HashSet;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {

        char[][] board2 = {
                {'.', '.', '.', '.', '5', '.', '.', '1', '.'},
                {'.', '4', '.', '3', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '3', '.', '.', '1'},
                {'8', '.', '.', '.', '.', '.', '.', '2', '.'},
                {'.', '.', '2', '.', '7', '.', '.', '.', '.'},
                {'.', '1', '5', '.', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '2', '.', '.', '.'},
                {'.', '2', '.', '9', '.', '.', '.', '.', '.'},
                {'.', '.', '4', '.', '.', '.', '.', '.', '.'},
        };


        char[][] board = {
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'},
        };
        System.out.println(isValidSudoku(board2));
    }

    private boolean isValidSudoku(char[][] board) {
        for (int i=0; i<board.length; i++){
            HashSet<Character> rows = new HashSet<>();
            HashSet<Character> columns = new HashSet<>();
            HashSet<Character> squares = new HashSet<>();
            for (int j = 0; j < board.length; j++) {
                if (board[i][j]!='.' && !rows.add(board[i][j]))
                    return false;
                if (board[j][i]!='.' && !columns.add(board[j][i]))
                    return false;
                int rowCount  = 3*(i/3);
                int columnCount = 3*(i%3);
                if (board[rowCount + j/3][columnCount+j%3]!='.'&& !squares.add(board[rowCount + j/3][columnCount+j%3]))
                    return false;
            }
        }
        return true;
    }
    
    private boolean isValidSudoku2(char[][] board){
        Set seen = new HashSet();
        for(int i=0; i<9; i++){
            for(int j=0; j<9; j++){
                char number = board[i][j];
                if(number!='.')
                    if(!seen.add(number + " in row " + i) ||
                       !seen.add(number + " in column " + j) ||
                       !seen.add(number + " in block " + i/3 + ":" + j/3))
                            return false;
            }
        }
        return true;
    }
}
